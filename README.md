## Josh's spelling helper.
This takes in a text file, creates a random sentence using the words in the 
text file, reads the sentence out to the user and then they try to rewrite the
sentence. They are then told every word they spelt incorrectly and those words
have an increased probability of appearing in later random sentences.

There are two different setups for this code. The one on the main page uses 
natural language processing to create sentences. This code identifies the word
type of the incorrectly spelt word and then constructs sentences based on English
sentence structures such as Subject => Verb => Object.

The second uses the Markov Method set with an order of 3. The Markov class was
made by Phil Wilson who adapted the code from Omer Nevo. This means that it looks
at all three consecutive words in the text and finds all the words that come after
those strings of words. Then it picks a random set of 3 consecutive words 
(word 1, word 2, word 3), takes a random word that follows the set in the text
(word 4), then picks random word (word 5) that follows 3 consecutive words 
(word 2, word 3, word 4) and then repeats this method until a sentence with 7
words is created.
