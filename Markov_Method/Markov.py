import random
from nltk import pos_tag

class Markov:
    def __init__(self, order):
        self.order = order
        self.group_size = self.order + 1
        self.author_text = None
        self.next_word_for = {}


    def train(self, text):
        self.author_text = text

        for i in range(0, len(self.author_text) - self.group_size):
            key = tuple(self.author_text[i:i + self.order])
            value = self.author_text[i + self.order]

            if key in self.next_word_for:
                self.next_word_for[key].append(value)
            else:
                self.next_word_for[key] = [value]


    def generate(self, length):
        # choose random starting sequence of order length
        index = random.randint(0, len(self.author_text) - self.order)
        result = self.author_text[index:index+self.order]

        # iteratively look up list of the possible words given the
        # last order elements of our result list
        # choose one at random and append it to the result list
        #print(self.next_word_for)
        for i in range(length):
            state = tuple(result[len(result) - self.order:])
            next_word = random.choice(self.next_word_for[state])
            result.append(next_word)
        x=""
        for i in range(len(result)):
            if i==0 or pos_tag([result[i],'hits'])[0][1]=='NNP' or pos_tag([result[i],'hit'])[0][1]=='NNPS' or result[i].lower()=='i' or result[i-1][-1]=="." or result[i-1][-1]=="?" or result[i-1][-1]=="!":
                x+=result[i].capitalize()
                x+=" "
            else:
                x+=result[i].lower()
                x+=" "
        return x
        #return " ".join(result[self.order:])

    def getNextWordFor(self):
        return self.next_word_for
