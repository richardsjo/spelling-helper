from gtts import gTTS
from hashlib import sha1
import os
import string



# # Functions that makes an mp3 from a message and then plays it.
# def tts(message, lang = 'en'):
#     str(lang)
#     tts = gTTS(text=message, lang=lang)
#     a = message.replace(" ", "")
#     tts.save("sounds/" + a + ".mp3")
#     os.system("mpg321 sounds/" + a + ".mp3")

# Same as above but saves as hash.
def ttsHash(message, lang = 'en'):
    str(lang)
    tts = gTTS(text=message, lang=lang)
    a = sha1(message.encode('utf-8')).hexdigest()
    tts.save("sounds/" + a + ".mp3")
    os.system("mpg321 -q sounds/" + a + ".mp3")
#ttsHash('good morning, josh', 'cy')
